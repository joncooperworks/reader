package com.cooperthecoder.reader.azure.models


data class Category(
        val name: String,
        val score: Double,
        val detail: Detail
)