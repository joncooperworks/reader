package com.cooperthecoder.reader.azure.models

data class Caption(
        val text: String,
        val confidence: Double
)