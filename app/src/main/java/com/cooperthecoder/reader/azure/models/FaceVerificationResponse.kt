package com.cooperthecoder.reader.azure.models

data class FaceVerificationResponse(
        val isIdentical: Boolean,
        val confidence: Float
)