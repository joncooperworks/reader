package com.cooperthecoder.reader.azure.models

data class Metadata(
        val width: Int,
        val height: Int,
        val format: String
)