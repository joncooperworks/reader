package com.cooperthecoder.reader.azure.models

class Line(
    val boundingBox: String,
    val words: List<Word>
)
