package com.cooperthecoder.reader.azure.models

data class Landmark(
        val name: String,
        val confidence: Double
)