package com.cooperthecoder.reader.azure.models

class Description(
        val tags: List<String>,
        val captions: List<Caption>
)