package com.cooperthecoder.reader.azure.models

data class Detail(
        val celebrities: List<Celebrity>,
        val landmarks: List<Landmark>
)