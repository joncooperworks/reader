package com.cooperthecoder.reader.azure.models

class Face(
        val age: Int,
        val gender: String,
        val faceRectangle: FaceRectangle
)