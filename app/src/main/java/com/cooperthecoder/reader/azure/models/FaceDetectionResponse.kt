package com.cooperthecoder.reader.azure.models

data class FaceDetectionResponse(
        val faceId: String,
        val faceRectangle: FaceRectangle
)