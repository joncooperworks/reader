package com.cooperthecoder.reader.azure.models

data class Tag(
    val name: String,
    val confidence: Double
)