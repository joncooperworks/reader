package com.cooperthecoder.reader.azure.models

data class Region(
        val boundingBox: String,
        val lines: List<Line>
)
