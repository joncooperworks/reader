package com.cooperthecoder.reader.azure.models

data class Word(
        val boundingBox: String,
        val text: String
)
