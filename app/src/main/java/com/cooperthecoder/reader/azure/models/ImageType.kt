package com.cooperthecoder.reader.azure.models

data class ImageType(
    val clipArtType: Int,
    val lineDrawingType: Int
)