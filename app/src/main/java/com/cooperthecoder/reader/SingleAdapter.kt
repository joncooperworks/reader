package com.cooperthecoder.reader

import io.fotoapparat.result.adapter.Adapter
import io.reactivex.Single
import java.util.concurrent.Future
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class SingleAdapter<T>: Adapter<T, Single<T>> {

    companion object {
        fun <R> toSingle(): SingleAdapter<R> {
            return SingleAdapter()
        }
    }

    override fun adapt(future: Future<T>): Single<T> {
        return Single.fromFuture(future)
    }
}